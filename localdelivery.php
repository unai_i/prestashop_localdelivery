<?php
/****************************************************************************
*  Copyright 2016 Unai IRIGOYEN <u.irigoyen@gmail.com>                      *
*                                                                           *
*  This program is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published by     *
*  the Free Software Foundation, either version 3 of the License, or        *
*  (at your option) any later version.                                      *
*                                                                           *
*  This program is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*  GNU General Public License for more details.                             *
*                                                                           *
*  You should have received a copy of the GNU General Public License        *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
****************************************************************************/

/****************************************************************************************************************
* Based on: https://www.prestashop.com/blog/fr/les_modules_transporteurs_fonctionnement_creation_configuration/ *
****************************************************************************************************************/


// Avoid direct access to the file
if (!defined('_PS_VERSION_'))
	exit;

class localdelivery extends CarrierModule
{
	public  $id_carrier;

	private $_html = '';
	private $_postErrors = array();
	private $_moduleName = 'localdelivery';


	/*
	** Construct Method
	**
	*/

	public function __construct()
	{
		$this->name = 'localdelivery';
		$this->tab = 'shipping_logistics';
		$this->version = '1.0';
		$this->author = 'Unai IRIGOYEN';
		$this->limited_countries = array('fr');

		parent::__construct ();

		$this->displayName = $this->l('Local delivery');
		$this->description = $this->l('Offer convenient delivery to local customers.');

		if (self::isInstalled($this->name))
		{
			// Getting carrier list
			global $cookie;
			$carriers = Carrier::getCarriers($cookie->id_lang, true, false, false, NULL, PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE);

			// Saving id carrier list
			$id_carrier_list = array();
			foreach($carriers as $carrier)
				$id_carrier_list[] .= $carrier['id_carrier'];

			// Testing if Carrier Id exists
			$warning = array();
			if (!in_array((int)(Configuration::get('LOCALDELIVERY_CARRIER_ID')), $id_carrier_list))
				$warning[] .= $this->l('"Local Delivery"').' ';
			if (!Configuration::get('LOCALDELIVERY_OVERCOST'))
				$warning[] .= $this->l('"Local Delivery Overcost"').' ';
			if (!Configuration::get('LOCALDELIVERY_POSTCODES'))
				$warning[] .= $this->l('"Local Delivery Post Codes"').' ';
			if (count($warning))
				$this->warning .= implode(' , ',$warning).$this->l('must be configured to use this module correctly').' ';
		}
	}


	/*
	** Install / Uninstall Methods
	**
	*/

	public function install()
	{
		$carrierConfig = array(
			0 => array('name' => 'Livraison locale',
				'id_tax_rules_group' => 0,
				'active' => true,
				'deleted' => 0,
				'shipping_handling' => false,
				'range_behavior' => 0,
				'delay' => array('fr' => 'Livraison en main propre aux alentours.', 'en' => 'Hand to hand delivery for local customers.', Language::getIsoById(Configuration::get('PS_LANG_DEFAULT')) => 'Description 1'),
				'id_zone' => 1,
				'is_module' => true,
				'shipping_external' => true,
				'external_module_name' => 'localdelivery',
				'need_range' => true
			),
		);

		$id_carrier1 = $this->installExternalCarrier($carrierConfig[0]);
		Configuration::updateValue('LOCALDELIVERY_CARRIER_ID', (int)$id_carrier1);
		if (!parent::install() ||
		    !Configuration::updateValue('LOCALDELIVERY_OVERCOST', '') ||
		    !Configuration::updateValue('LOCALDELIVERY_POSTCODES', '') ||
		    !$this->registerHook('updateCarrier'))
			return false;
		return true;
	}
	
	public function uninstall()
	{
		// Uninstall
		if (!parent::uninstall() ||
		    !Configuration::deleteByName('LOCALDELIVERY_OVERCOST') ||
		    !Configuration::deleteByName('LOCALDELIVERY_POSTCODES') ||
		    !$this->unregisterHook('updateCarrier'))
			return false;
		
		// Delete External Carrier
		$Carrier1 = new Carrier((int)(Configuration::get('LOCALDELIVERY_CARRIER_ID')));

		// If external carrier is default set other one as default
		if (Configuration::get('PS_CARRIER_DEFAULT') == (int)($Carrier1->id))
		{
			global $cookie;
			$carriersD = Carrier::getCarriers($cookie->id_lang, true, false, false, NULL, PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE);
			foreach($carriersD as $carrierD)
				if ($carrierD['active'] AND !$carrierD['deleted'] AND ($carrierD['name'] != $this->_config['name']))
					Configuration::updateValue('PS_CARRIER_DEFAULT', $carrierD['id_carrier']);
		}

		// Then delete Carrier
		$Carrier1->deleted = 1;
		if (!$Carrier1->update())
			return false;

		return true;
	}

	public static function installExternalCarrier($config)
	{
		$carrier = new Carrier();
		$carrier->name = $config['name'];
		$carrier->id_tax_rules_group = $config['id_tax_rules_group'];
		$carrier->id_zone = $config['id_zone'];
		$carrier->active = $config['active'];
		$carrier->deleted = $config['deleted'];
		$carrier->delay = $config['delay'];
		$carrier->shipping_handling = $config['shipping_handling'];
		$carrier->range_behavior = $config['range_behavior'];
		$carrier->is_module = $config['is_module'];
		$carrier->shipping_external = $config['shipping_external'];
		$carrier->external_module_name = $config['external_module_name'];
		$carrier->need_range = $config['need_range'];

		$languages = Language::getLanguages(true);
		foreach ($languages as $language)
		{
			if ($language['iso_code'] == 'fr')
				$carrier->delay[(int)$language['id_lang']] = $config['delay'][$language['iso_code']];
			if ($language['iso_code'] == 'en')
				$carrier->delay[(int)$language['id_lang']] = $config['delay'][$language['iso_code']];
			if ($language['iso_code'] == Language::getIsoById(Configuration::get('PS_LANG_DEFAULT')))
				$carrier->delay[(int)$language['id_lang']] = $config['delay'][$language['iso_code']];
		}

		if ($carrier->add())
		{
			$groups = Group::getGroups(true);
			foreach ($groups as $group)
				Db::getInstance()->autoExecute(_DB_PREFIX_.'carrier_group', array('id_carrier' => (int)($carrier->id), 'id_group' => (int)($group['id_group'])), 'INSERT');

			$zones = Zone::getZones(true);
			foreach ($zones as $zone)
			{
				Db::getInstance()->autoExecute(_DB_PREFIX_.'carrier_zone', array('id_carrier' => (int)($carrier->id), 'id_zone' => (int)($zone['id_zone'])), 'INSERT');
			}

			// Copy Logo
			if (!copy(dirname(__FILE__).'/carrier.jpg', _PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg'))
				return false;

			// Return ID Carrier
			return (int)($carrier->id);
		}

		return false;
	}




	/*
	** Form Config Methods
	**
	*/

	public function getContent()
	{
		if (!empty($_POST) AND Tools::isSubmit('submitSave'))
		{
			$this->_postValidation();
			if (!sizeof($this->_postErrors))
				$this->_postProcess();
			else
				foreach ($this->_postErrors AS $err)
					$this->_html .= $this->displayError('<img src="'._PS_IMG_.'admin/forbbiden.gif" alt="nok" />&nbsp;'.$err);
		}
		return $this->_html.$this->renderForm();
	}

	private function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Local Delivery Module Settings'),
					'image' => $this->_path.'logo.gif',
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Overcost'),
						'name' => 'localdelivery_overcost',
						'size'     => 10,
						'required' => true,
						'desc' => $this->l('Delivery overcost'),
						'suffix' => 'euros',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Post Codes'),
						'name' => 'localdelivery_postcodes',
						'size'     => 50,
						'required' => true,
						'desc' => $this->l('List of post codes for which delivery applies'),
						'hint' => $this->l('Comma (,) separated list of post codes'),
					),
				),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button')
			),
		);
		
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang =
			Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
	
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitSave';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
		.'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => array(
				'localdelivery_overcost' => Configuration::get('LOCALDELIVERY_OVERCOST'),
				'localdelivery_postcodes' => Configuration::get('LOCALDELIVERY_POSTCODES'),
			),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
	
		return $helper->generateForm(array($fields_form));
	}

	private function _postValidation()
	{
		// Check configuration values
		if (Tools::getValue('localdelivery_overcost') == '' || Tools::getValue('localdelivery_postcodes') == '')
			$this->_postErrors[]  = $this->l('You have to configure overcost and postcodes');
		if (!is_numeric(Tools::getValue('localdelivery_overcost')))
			$this->_postErrors[]  = $this->l('Overcost must be a numeric value');
		if (!preg_match('/^(?:\d{5},?)+$/', Tools::getValue('localdelivery_postcodes')))
			$this->_postErrors[]  = $this->l('There is at least one invalid post code');
	}

	private function _postProcess()
	{
		// Saving new configurations
		if (Configuration::updateValue('LOCALDELIVERY_OVERCOST', Tools::getValue('localdelivery_overcost')) &&
		    Configuration::updateValue('LOCALDELIVERY_POSTCODES', Tools::getValue('localdelivery_postcodes')))
			$this->_html .= $this->displayConfirmation($this->l('Settings updated'));
		else
			$this->_html .= $this->displayErrors($this->l('Settings failed'));
	}


	/*
	** Hook update carrier
	**
	*/

	public function hookupdateCarrier($params)
	{
		if ((int)($params['id_carrier']) == (int)(Configuration::get('LOCALDELIVERY_CARRIER_ID')))
			Configuration::updateValue('LOCALDELIVERY_CARRIER_ID', (int)($params['carrier']->id));
	}




	/*
	** Front Methods
	**
	** If you set need_range at true when you created your carrier (in install method), the method called by the cart will be getOrderShippingCost
	** If not, the method called will be getOrderShippingCostExternal
	**
	** $params var contains the cart, the customer, the address
	** $shipping_cost var contains the price calculated by the range in carrier tab
	**
	*/
	
	public function getOrderShippingCost($params, $shipping_cost)
	{
		// This example returns shipping cost with overcost set in the back-office, but you can call a webservice or calculate what you want before returning the final value to the Cart
		if ($this->id_carrier == (int)(Configuration::get('LOCALDELIVERY_CARRIER_ID')))
		{
			$address = new Address((int)$params->id_address_delivery);
			if(!Validate::isLoadedObject($address))
				return false;
			$localPostcodes = [];
			preg_match_all('/\d{5}/', Configuration::get('LOCALDELIVERY_POSTCODES'), $localPostcodes);
			if(in_array($address->postcode, $localPostcodes[0]))
				return (float)(Configuration::get('LOCALDELIVERY_OVERCOST')) + $shipping_cost;
		}

		// If the carrier is not known, you can return false, the carrier won't appear in the order process
		return false;
	}
	
	public function getOrderShippingCostExternal($params)
	{
		// This example returns the overcost directly, but you can call a webservice or calculate what you want before returning the final value to the Cart
		if ($this->id_carrier == (int)(Configuration::get('LOCALDELIVERY_CARRIER_ID')))
		{
			$address = new Address((int)$params->id_address_delivery);
			if(!Validate::isLoadedObject($address))
				return false;
			$localPostcodes = [];
			preg_match_all('/\d{5}/', Configuration::get('LOCALDELIVERY_POSTCODES'), $localPostcodes);
			if(in_array($address->postcode, $localPostcodes[0]))
				return (float)(Configuration::get('LOCALDELIVERY_OVERCOST'));
		}

		// If the carrier is not known, you can return false, the carrier won't appear in the order process
		return false;
	}
	
}


